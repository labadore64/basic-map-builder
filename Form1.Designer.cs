﻿
namespace MapBuilder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CreateItemMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createCollisionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.createAXLabelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createAXLabelAreaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.createSoundEffectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.createWarpAreaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createWarpDestinationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.setSpawnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AXLabelContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateDescriptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SoundEffectContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.CollisionContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.AXLabelAreaContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addPointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removePointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.renameToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.changeDescriptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteAXLabelAreaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.WarpDestinationContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteDestinationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WarpAreaContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addPointToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deletePointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.changeDestinationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteWarpAreaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioSpawn = new System.Windows.Forms.RadioButton();
            this.radioWarpDestination = new System.Windows.Forms.RadioButton();
            this.radioWarpEnter = new System.Windows.Forms.RadioButton();
            this.radioSoundEffect = new System.Windows.Forms.RadioButton();
            this.radioCollision = new System.Windows.Forms.RadioButton();
            this.radioAXLabelArea = new System.Windows.Forms.RadioButton();
            this.radioAXLabel = new System.Windows.Forms.RadioButton();
            this.radioAny = new System.Windows.Forms.RadioButton();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator19 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.mapBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CreateItemMenu.SuspendLayout();
            this.AXLabelContextMenu.SuspendLayout();
            this.SoundEffectContextMenu.SuspendLayout();
            this.CollisionContextMenu.SuspendLayout();
            this.AXLabelAreaContextMenu.SuspendLayout();
            this.WarpDestinationContextMenu.SuspendLayout();
            this.WarpAreaContextMenu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mapBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // CreateItemMenu
            // 
            this.CreateItemMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createCollisionToolStripMenuItem,
            this.toolStripSeparator1,
            this.createAXLabelToolStripMenuItem,
            this.createAXLabelAreaToolStripMenuItem,
            this.toolStripSeparator2,
            this.createSoundEffectToolStripMenuItem,
            this.toolStripSeparator8,
            this.createWarpAreaToolStripMenuItem,
            this.createWarpDestinationToolStripMenuItem,
            this.toolStripSeparator15,
            this.setSpawnToolStripMenuItem});
            this.CreateItemMenu.Name = "CreateItemMenu";
            this.CreateItemMenu.Size = new System.Drawing.Size(203, 182);
            // 
            // createCollisionToolStripMenuItem
            // 
            this.createCollisionToolStripMenuItem.Name = "createCollisionToolStripMenuItem";
            this.createCollisionToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.createCollisionToolStripMenuItem.Text = "Create Collision";
            this.createCollisionToolStripMenuItem.Click += new System.EventHandler(this.CreateCollisionToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(199, 6);
            // 
            // createAXLabelToolStripMenuItem
            // 
            this.createAXLabelToolStripMenuItem.Name = "createAXLabelToolStripMenuItem";
            this.createAXLabelToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.createAXLabelToolStripMenuItem.Text = "Create AXLabel";
            this.createAXLabelToolStripMenuItem.Click += new System.EventHandler(this.CreateAXLabelToolStripMenuItem_Click);
            // 
            // createAXLabelAreaToolStripMenuItem
            // 
            this.createAXLabelAreaToolStripMenuItem.Name = "createAXLabelAreaToolStripMenuItem";
            this.createAXLabelAreaToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.createAXLabelAreaToolStripMenuItem.Text = "Create AXLabel Area";
            this.createAXLabelAreaToolStripMenuItem.Click += new System.EventHandler(this.CreateAXLabelAreaToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(199, 6);
            // 
            // createSoundEffectToolStripMenuItem
            // 
            this.createSoundEffectToolStripMenuItem.Name = "createSoundEffectToolStripMenuItem";
            this.createSoundEffectToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.createSoundEffectToolStripMenuItem.Text = "Create Sound Effect";
            this.createSoundEffectToolStripMenuItem.Click += new System.EventHandler(this.CreateSoundEffectToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(199, 6);
            // 
            // createWarpAreaToolStripMenuItem
            // 
            this.createWarpAreaToolStripMenuItem.Name = "createWarpAreaToolStripMenuItem";
            this.createWarpAreaToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.createWarpAreaToolStripMenuItem.Text = "Create Warp Area";
            this.createWarpAreaToolStripMenuItem.Click += new System.EventHandler(this.CreateWarpAreaToolStripMenuItem_Click);
            // 
            // createWarpDestinationToolStripMenuItem
            // 
            this.createWarpDestinationToolStripMenuItem.Name = "createWarpDestinationToolStripMenuItem";
            this.createWarpDestinationToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.createWarpDestinationToolStripMenuItem.Text = "Create Warp Destination";
            this.createWarpDestinationToolStripMenuItem.Click += new System.EventHandler(this.CreateWarpDestinationToolStripMenuItem_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(199, 6);
            // 
            // setSpawnToolStripMenuItem
            // 
            this.setSpawnToolStripMenuItem.Name = "setSpawnToolStripMenuItem";
            this.setSpawnToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.setSpawnToolStripMenuItem.Text = "Set Spawn";
            this.setSpawnToolStripMenuItem.Click += new System.EventHandler(this.SetSpawnToolStripMenuItem_Click);
            // 
            // AXLabelContextMenu
            // 
            this.AXLabelContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.renameToolStripMenuItem,
            this.updateDescriptionToolStripMenuItem,
            this.toolStripSeparator13,
            this.toolStripMenuItem9,
            this.toolStripSeparator12,
            this.toolStripMenuItem6,
            this.toolStripSeparator3,
            this.deleteToolStripMenuItem});
            this.AXLabelContextMenu.Name = "AXLabelContextMenu";
            this.AXLabelContextMenu.Size = new System.Drawing.Size(222, 132);
            this.AXLabelContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.AXLabelContextMenu_Opening);
            // 
            // renameToolStripMenuItem
            // 
            this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
            this.renameToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.renameToolStripMenuItem.Text = "Rename AXLabel";
            this.renameToolStripMenuItem.Click += new System.EventHandler(this.RenameToolStripMenuItem_Click);
            // 
            // updateDescriptionToolStripMenuItem
            // 
            this.updateDescriptionToolStripMenuItem.Name = "updateDescriptionToolStripMenuItem";
            this.updateDescriptionToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.updateDescriptionToolStripMenuItem.Text = "Update AXLabel Description";
            this.updateDescriptionToolStripMenuItem.Click += new System.EventHandler(this.UpdateDescriptionToolStripMenuItem_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(218, 6);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(221, 22);
            this.toolStripMenuItem9.Text = "Update Radius";
            this.toolStripMenuItem9.Click += new System.EventHandler(this.ToolStripMenuItem9_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(218, 6);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.deleteToolStripMenuItem.Text = "Delete AXLabel";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.DeleteToolStripMenuItem_Click);
            // 
            // SoundEffectContextMenu
            // 
            this.SoundEffectContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripSeparator14,
            this.toolStripMenuItem10,
            this.toolStripSeparator16,
            this.toolStripMenuItem7,
            this.toolStripSeparator4,
            this.toolStripMenuItem3});
            this.SoundEffectContextMenu.Name = "AXLabelContextMenu";
            this.SoundEffectContextMenu.Size = new System.Drawing.Size(188, 110);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItem1.Text = "Rename Sound Effect";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem1_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(184, 6);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItem10.Text = "Update Radius";
            this.toolStripMenuItem10.Click += new System.EventHandler(this.ToolStripMenuItem10_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(184, 6);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItem3.Text = "Delete Sound Effect";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.ToolStripMenuItem3_Click);
            // 
            // CollisionContextMenu
            // 
            this.CollisionContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem5,
            this.toolStripSeparator17,
            this.toolStripMenuItem11,
            this.toolStripSeparator5,
            this.toolStripMenuItem4});
            this.CollisionContextMenu.Name = "AXLabelContextMenu";
            this.CollisionContextMenu.Size = new System.Drawing.Size(157, 104);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(156, 22);
            this.toolStripMenuItem2.Text = "Add Point";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.ToolStripMenuItem2_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(156, 22);
            this.toolStripMenuItem5.Text = "Remove Point";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.ToolStripMenuItem5_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(153, 6);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(156, 22);
            this.toolStripMenuItem4.Text = "Delete Collision";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.ToolStripMenuItem4_Click);
            // 
            // AXLabelAreaContextMenu
            // 
            this.AXLabelAreaContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addPointToolStripMenuItem,
            this.removePointToolStripMenuItem,
            this.toolStripSeparator6,
            this.renameToolStripMenuItem1,
            this.changeDescriptionToolStripMenuItem,
            this.toolStripSeparator18,
            this.toolStripMenuItem12,
            this.toolStripSeparator7,
            this.deleteAXLabelAreaToolStripMenuItem});
            this.AXLabelAreaContextMenu.Name = "AXLabelAreaContextMenu";
            this.AXLabelAreaContextMenu.Size = new System.Drawing.Size(181, 176);
            // 
            // addPointToolStripMenuItem
            // 
            this.addPointToolStripMenuItem.Name = "addPointToolStripMenuItem";
            this.addPointToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addPointToolStripMenuItem.Text = "Add Point";
            this.addPointToolStripMenuItem.Click += new System.EventHandler(this.AddPointToolStripMenuItem_Click);
            // 
            // removePointToolStripMenuItem
            // 
            this.removePointToolStripMenuItem.Name = "removePointToolStripMenuItem";
            this.removePointToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.removePointToolStripMenuItem.Text = "Remove Point";
            this.removePointToolStripMenuItem.Click += new System.EventHandler(this.RemovePointToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(177, 6);
            // 
            // renameToolStripMenuItem1
            // 
            this.renameToolStripMenuItem1.Name = "renameToolStripMenuItem1";
            this.renameToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.renameToolStripMenuItem1.Text = "Rename";
            this.renameToolStripMenuItem1.Click += new System.EventHandler(this.RenameToolStripMenuItem1_Click);
            // 
            // changeDescriptionToolStripMenuItem
            // 
            this.changeDescriptionToolStripMenuItem.Name = "changeDescriptionToolStripMenuItem";
            this.changeDescriptionToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.changeDescriptionToolStripMenuItem.Text = "Change Description";
            this.changeDescriptionToolStripMenuItem.Click += new System.EventHandler(this.ChangeDescriptionToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(177, 6);
            // 
            // deleteAXLabelAreaToolStripMenuItem
            // 
            this.deleteAXLabelAreaToolStripMenuItem.Name = "deleteAXLabelAreaToolStripMenuItem";
            this.deleteAXLabelAreaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.deleteAXLabelAreaToolStripMenuItem.Text = "Delete AXLabel Area";
            this.deleteAXLabelAreaToolStripMenuItem.Click += new System.EventHandler(this.DeleteAXLabelAreaToolStripMenuItem_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 331);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(66, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "New";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 22);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(138, 303);
            this.listBox1.TabIndex = 6;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.ListBox1_SelectedIndexChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(84, 331);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(66, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Delete";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Location = new System.Drawing.Point(156, 22);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1066, 623);
            this.panel1.TabIndex = 10;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel1_Paint);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(12, 360);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(66, 23);
            this.button5.TabIndex = 11;
            this.button5.Text = "Rename";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.Button5_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(12, 389);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(66, 23);
            this.button3.TabIndex = 12;
            this.button3.Text = "Save";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(84, 389);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(66, 23);
            this.button4.TabIndex = 13;
            this.button4.Text = "Load";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "xml";
            this.saveFileDialog1.FileName = "LevelData.xml";
            this.saveFileDialog1.Filter = "XML Files|*.xml";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.SaveFileDialog1_FileOk);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "XML Files|*.xml";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.OpenFileDialog1_FileOk);
            // 
            // WarpDestinationContextMenu
            // 
            this.WarpDestinationContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem8,
            this.toolStripSeparator19,
            this.toolStripMenuItem13,
            this.toolStripSeparator11,
            this.deleteDestinationToolStripMenuItem});
            this.WarpDestinationContextMenu.Name = "AXLabelContextMenu";
            this.WarpDestinationContextMenu.Size = new System.Drawing.Size(171, 82);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(170, 22);
            this.toolStripMenuItem8.Text = "Update ID";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.ToolStripMenuItem8_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(167, 6);
            // 
            // deleteDestinationToolStripMenuItem
            // 
            this.deleteDestinationToolStripMenuItem.Name = "deleteDestinationToolStripMenuItem";
            this.deleteDestinationToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.deleteDestinationToolStripMenuItem.Text = "Delete Destination";
            this.deleteDestinationToolStripMenuItem.Click += new System.EventHandler(this.DeleteDestinationToolStripMenuItem_Click);
            // 
            // WarpAreaContextMenu
            // 
            this.WarpAreaContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addPointToolStripMenuItem1,
            this.deletePointToolStripMenuItem,
            this.toolStripSeparator9,
            this.changeDestinationToolStripMenuItem,
            this.toolStripSeparator20,
            this.toolStripMenuItem14,
            this.toolStripSeparator10,
            this.deleteWarpAreaToolStripMenuItem});
            this.WarpAreaContextMenu.Name = "WarpAreaContextMenu";
            this.WarpAreaContextMenu.Size = new System.Drawing.Size(179, 132);
            // 
            // addPointToolStripMenuItem1
            // 
            this.addPointToolStripMenuItem1.Name = "addPointToolStripMenuItem1";
            this.addPointToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.addPointToolStripMenuItem1.Text = "Add Point";
            this.addPointToolStripMenuItem1.Click += new System.EventHandler(this.AddPointToolStripMenuItem1_Click);
            // 
            // deletePointToolStripMenuItem
            // 
            this.deletePointToolStripMenuItem.Name = "deletePointToolStripMenuItem";
            this.deletePointToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.deletePointToolStripMenuItem.Text = "Delete Point";
            this.deletePointToolStripMenuItem.Click += new System.EventHandler(this.DeletePointToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(177, 6);
            // 
            // changeDestinationToolStripMenuItem
            // 
            this.changeDestinationToolStripMenuItem.Name = "changeDestinationToolStripMenuItem";
            this.changeDestinationToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.changeDestinationToolStripMenuItem.Text = "Change Destination";
            this.changeDestinationToolStripMenuItem.Click += new System.EventHandler(this.ChangeDestinationToolStripMenuItem_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(177, 6);
            // 
            // deleteWarpAreaToolStripMenuItem
            // 
            this.deleteWarpAreaToolStripMenuItem.Name = "deleteWarpAreaToolStripMenuItem";
            this.deleteWarpAreaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.deleteWarpAreaToolStripMenuItem.Text = "Delete Warp Area";
            this.deleteWarpAreaToolStripMenuItem.Click += new System.EventHandler(this.DeleteWarpAreaToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioSpawn);
            this.groupBox1.Controls.Add(this.radioWarpDestination);
            this.groupBox1.Controls.Add(this.radioWarpEnter);
            this.groupBox1.Controls.Add(this.radioSoundEffect);
            this.groupBox1.Controls.Add(this.radioCollision);
            this.groupBox1.Controls.Add(this.radioAXLabelArea);
            this.groupBox1.Controls.Add(this.radioAXLabel);
            this.groupBox1.Controls.Add(this.radioAny);
            this.groupBox1.Location = new System.Drawing.Point(12, 418);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(138, 227);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Selection";
            // 
            // radioSpawn
            // 
            this.radioSpawn.AutoSize = true;
            this.radioSpawn.Location = new System.Drawing.Point(7, 187);
            this.radioSpawn.Name = "radioSpawn";
            this.radioSpawn.Size = new System.Drawing.Size(85, 17);
            this.radioSpawn.TabIndex = 7;
            this.radioSpawn.TabStop = true;
            this.radioSpawn.Text = "Spawn Point";
            this.radioSpawn.UseVisualStyleBackColor = true;
            this.radioSpawn.CheckedChanged += new System.EventHandler(this.RadioSpawn_CheckedChanged);
            // 
            // radioWarpDestination
            // 
            this.radioWarpDestination.AutoSize = true;
            this.radioWarpDestination.Location = new System.Drawing.Point(7, 163);
            this.radioWarpDestination.Name = "radioWarpDestination";
            this.radioWarpDestination.Size = new System.Drawing.Size(112, 17);
            this.radioWarpDestination.TabIndex = 6;
            this.radioWarpDestination.Text = "Warp Destinations";
            this.radioWarpDestination.UseVisualStyleBackColor = true;
            this.radioWarpDestination.CheckedChanged += new System.EventHandler(this.RadioWarpDestination_CheckedChanged);
            // 
            // radioWarpEnter
            // 
            this.radioWarpEnter.AutoSize = true;
            this.radioWarpEnter.Location = new System.Drawing.Point(7, 140);
            this.radioWarpEnter.Name = "radioWarpEnter";
            this.radioWarpEnter.Size = new System.Drawing.Size(102, 17);
            this.radioWarpEnter.TabIndex = 5;
            this.radioWarpEnter.Text = "Warp Entrances";
            this.radioWarpEnter.UseVisualStyleBackColor = true;
            this.radioWarpEnter.CheckedChanged += new System.EventHandler(this.RadioWarpEnter_CheckedChanged);
            // 
            // radioSoundEffect
            // 
            this.radioSoundEffect.AutoSize = true;
            this.radioSoundEffect.Location = new System.Drawing.Point(7, 116);
            this.radioSoundEffect.Name = "radioSoundEffect";
            this.radioSoundEffect.Size = new System.Drawing.Size(92, 17);
            this.radioSoundEffect.TabIndex = 4;
            this.radioSoundEffect.Text = "Sound Effects";
            this.radioSoundEffect.UseVisualStyleBackColor = true;
            this.radioSoundEffect.CheckedChanged += new System.EventHandler(this.RadioSoundEffect_CheckedChanged);
            // 
            // radioCollision
            // 
            this.radioCollision.AutoSize = true;
            this.radioCollision.Location = new System.Drawing.Point(7, 92);
            this.radioCollision.Name = "radioCollision";
            this.radioCollision.Size = new System.Drawing.Size(68, 17);
            this.radioCollision.TabIndex = 3;
            this.radioCollision.Text = "Collisions";
            this.radioCollision.UseVisualStyleBackColor = true;
            this.radioCollision.CheckedChanged += new System.EventHandler(this.RadioCollision_CheckedChanged);
            // 
            // radioAXLabelArea
            // 
            this.radioAXLabelArea.AutoSize = true;
            this.radioAXLabelArea.Location = new System.Drawing.Point(7, 68);
            this.radioAXLabelArea.Name = "radioAXLabelArea";
            this.radioAXLabelArea.Size = new System.Drawing.Size(95, 17);
            this.radioAXLabelArea.TabIndex = 2;
            this.radioAXLabelArea.Text = "AXLabel Areas";
            this.radioAXLabelArea.UseVisualStyleBackColor = true;
            this.radioAXLabelArea.CheckedChanged += new System.EventHandler(this.RadioAXLabelArea_CheckedChanged);
            // 
            // radioAXLabel
            // 
            this.radioAXLabel.AutoSize = true;
            this.radioAXLabel.Location = new System.Drawing.Point(7, 44);
            this.radioAXLabel.Name = "radioAXLabel";
            this.radioAXLabel.Size = new System.Drawing.Size(70, 17);
            this.radioAXLabel.TabIndex = 1;
            this.radioAXLabel.Text = "AXLabels";
            this.radioAXLabel.UseVisualStyleBackColor = true;
            this.radioAXLabel.CheckedChanged += new System.EventHandler(this.RadioAXLabel_CheckedChanged);
            // 
            // radioAny
            // 
            this.radioAny.AutoSize = true;
            this.radioAny.Checked = true;
            this.radioAny.Location = new System.Drawing.Point(7, 20);
            this.radioAny.Name = "radioAny";
            this.radioAny.Size = new System.Drawing.Size(36, 17);
            this.radioAny.TabIndex = 0;
            this.radioAny.TabStop = true;
            this.radioAny.Text = "All";
            this.radioAny.UseVisualStyleBackColor = true;
            this.radioAny.CheckedChanged += new System.EventHandler(this.RadioAny_CheckedChanged);
            // 
            // toolTip
            // 
            this.toolTip.IsBalloon = true;
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(218, 6);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(221, 22);
            this.toolStripMenuItem6.Text = "Copy";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.ToolStripMenuItem6_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(184, 6);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItem7.Text = "Copy";
            this.toolStripMenuItem7.Click += new System.EventHandler(this.ToolStripMenuItem7_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(153, 6);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(156, 22);
            this.toolStripMenuItem11.Text = "Copy";
            this.toolStripMenuItem11.Click += new System.EventHandler(this.ToolStripMenuItem11_Click);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(177, 6);
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem12.Text = "Copy";
            this.toolStripMenuItem12.Click += new System.EventHandler(this.ToolStripMenuItem12_Click);
            // 
            // toolStripSeparator19
            // 
            this.toolStripSeparator19.Name = "toolStripSeparator19";
            this.toolStripSeparator19.Size = new System.Drawing.Size(167, 6);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(170, 22);
            this.toolStripMenuItem13.Text = "Copy";
            this.toolStripMenuItem13.Click += new System.EventHandler(this.ToolStripMenuItem13_Click);
            // 
            // toolStripSeparator20
            // 
            this.toolStripSeparator20.Name = "toolStripSeparator20";
            this.toolStripSeparator20.Size = new System.Drawing.Size(177, 6);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem14.Text = "Copy";
            this.toolStripMenuItem14.Click += new System.EventHandler(this.ToolStripMenuItem14_Click);
            // 
            // mapBindingSource
            // 
            this.mapBindingSource.DataSource = typeof(MapBuilder.objects.Map);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1234, 657);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            this.CreateItemMenu.ResumeLayout(false);
            this.AXLabelContextMenu.ResumeLayout(false);
            this.SoundEffectContextMenu.ResumeLayout(false);
            this.CollisionContextMenu.ResumeLayout(false);
            this.AXLabelAreaContextMenu.ResumeLayout(false);
            this.WarpDestinationContextMenu.ResumeLayout(false);
            this.WarpAreaContextMenu.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mapBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip CreateItemMenu;
        private System.Windows.Forms.ToolStripMenuItem createCollisionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem createAXLabelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createAXLabelAreaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem createSoundEffectToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip AXLabelContextMenu;
        private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateDescriptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip SoundEffectContextMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ContextMenuStrip CollisionContextMenu;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ContextMenuStrip AXLabelAreaContextMenu;
        private System.Windows.Forms.ToolStripMenuItem addPointToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removePointToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem changeDescriptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem deleteAXLabelAreaToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.BindingSource mapBindingSource;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem createWarpAreaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createWarpDestinationToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip WarpDestinationContextMenu;
        private System.Windows.Forms.ToolStripMenuItem deleteDestinationToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip WarpAreaContextMenu;
        private System.Windows.Forms.ToolStripMenuItem addPointToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deletePointToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem changeDestinationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem deleteWarpAreaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioWarpDestination;
        private System.Windows.Forms.RadioButton radioWarpEnter;
        private System.Windows.Forms.RadioButton radioSoundEffect;
        private System.Windows.Forms.RadioButton radioCollision;
        private System.Windows.Forms.RadioButton radioAXLabelArea;
        private System.Windows.Forms.RadioButton radioAXLabel;
        private System.Windows.Forms.RadioButton radioAny;
        private System.Windows.Forms.RadioButton radioSpawn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem setSpawnToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator19;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator20;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
    }
}

