﻿
using System.Drawing;
using System.Runtime.Serialization;
using System.Windows.Forms;

namespace MapBuilder.objects
{
    public class Collision : ShapeObject
    {

        public Collision(Panel form)
        {
            ParentForm = form;
            Color = Color.Red;
            InitializeBrush();
        }

        public Collision()
        {
            Color = Color.Red;
        }

        public override Drawable Copy()
        {
            Collision a = new Collision(ParentForm);

            a.Shape = CopyShape();

            return a;
        }
    }
}
