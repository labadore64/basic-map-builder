﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace MapBuilder.objects
{

    public class Map
    {

        [XmlElement("Spawn")]
        public Spawn Spawn { get; set; } = new Spawn();

        [XmlIgnore]
        public static int SetIndex { get { return i; } set { i = value; } }

        static int i = 0;
        [XmlAttribute("Name")]
        public string Name { get; set; } = "Default";

        [XmlArray("Collisions")]
        [XmlArrayItem("Collision")]
        public List<Collision> Collisions { get; set; } = new List<Collision>();
        [XmlArray("AXLabels")]
        [XmlArrayItem("AXLabel")]
        public List<AXLabel> Labels { get; set; } = new List<AXLabel>();
        [XmlArray("AXAreaLabels")]
        [XmlArrayItem("AXAreaLabel")]
        public List<AXLabelArea> AreaLabels { get; set; } = new List<AXLabelArea>();

        [XmlArray("WarpAreas")]
        [XmlArrayItem("WarpArea")]
        public List<WarpEnter> WarpEnters { get; set; } = new List<WarpEnter>();
        [XmlArray("WarpDestination")]
        [XmlArrayItem("WarpDestination")]
        public List<WarpDestination> WarpDestinations { get; set; } = new List<WarpDestination>();
        [XmlArray("Sounds")]
        [XmlArrayItem("Sound")]
        public List<SoundEffect> Sounds { get; set; } = new List<SoundEffect>();

        public Map()
        {
            i++;
            Name += i;
        }

        public override string ToString()
        {
            return Name;
        }

        public void SetParentForm(Panel g)
        {
            foreach(Collision c in Collisions)
            {
                c.ParentForm = g;
                c.InitializeBrush();
            }
            foreach (AXLabelArea c in AreaLabels)
            {
                c.ParentForm = g;
                c.InitializeBrush();
            }
            foreach (AXLabel c in Labels)
            {
                c.ParentForm = g;
            }
            foreach (SoundEffect c in Sounds)
            {
                c.ParentForm = g;
            }
            foreach (WarpEnter c in WarpEnters)
            {
                c.ParentForm = g;
                c.InitializeBrush();
            }
            foreach (WarpDestination c in WarpDestinations)
            {
                c.ParentForm = g;
                c.ParentMap = this;
            }
            Spawn.ParentForm = g;
        }
    }
}
