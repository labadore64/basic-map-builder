﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.Serialization;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace MapBuilder.objects
{
    public abstract class ShapeObject : Drawable
    {
        [XmlIgnore]
        const int DIST = 2;

        [XmlIgnore]
        public bool Selected { get; set; }
        [XmlIgnore]
        public Color Color { get; protected set; } = Color.Red;

        static Brush SelectedBrush = new SolidBrush(Color.White);
        static Brush NotSelectedBrush = new SolidBrush(Color.Black);

        [XmlIgnore]
        public Panel ParentForm { get; set; }
        public Point[] Shape { get; set; }

        [XmlIgnore]
        Point[] AdjustedShape
        {
            get
            {
                Point[] points = new Point[Shape.Length];
                for(int i = 0; i < Shape.Length; i++)
                {
                    points[i] = new Point(Shape[i].X + Camera.Position.X, Shape[i].Y + +Camera.Position.Y);
                }

                return points;
            }
        }

        [XmlIgnore]
        public Graphics g { get; protected set; }

        [XmlIgnore]
        public SolidBrush myBrush { get; protected set; }

        [XmlIgnore]
        public int BaseWidth { get; protected set; } = 50;
        [XmlIgnore]
        public int BaseHeight { get; protected set; } = 50;
        [XmlIgnore]
        public Point SelectedCorner {
            get {
                if (CenterSelected)
                {
                    return GetCenter();
                }
                else
                {
                    if(CornerIndex >= Shape.Length)
                    {
                        CornerIndex = 0;
                    }

                    return Shape[CornerIndex];
                }
            }
            set
            {
                if (CornerIndex >= Shape.Length)
                {
                    CornerIndex = 0;
                }
                Shape[CornerIndex] = value;
            }
        }
        [XmlIgnore]
        int CornerIndex { get; set; }
        [XmlIgnore]
        internal bool CenterSelected { get; set; }

        public void InitializeBrush()
        {
            g = ParentForm.CreateGraphics();
            myBrush = new SolidBrush(Color);
        }

        public void AddPoint(Point p)
        {
            List<Point> po = new List<Point>(Shape);

            po.Add(p);

            Shape = po.ToArray();
        }

        public void RemovePoint(Point p)
        {
            List<Point> po = new List<Point>(Shape);

            po.Remove(p);

            Shape = po.ToArray();
        }

        public void Draw()
        {

            g.FillPolygon(myBrush, AdjustedShape);

            foreach (Point p in AdjustedShape)
            {
                if (p != SelectedCorner)
                {
                    DrawVertex(p, NotSelectedBrush);
                } else
                {
                    DrawVertex(p, SelectedBrush);
                }
            }

            Point pa = GetCenter();
            pa.X += Camera.Position.X;
            pa.Y += Camera.Position.Y;
            if (pa != SelectedCorner)
            {
                DrawVertex(pa, NotSelectedBrush);
            }
            else
            {
                DrawVertex(pa, SelectedBrush);
            }

        }

        /// <summary>
        /// Gets the centre of the polygon
        /// </summary>
        /// <returns>The center</returns>
        public Point GetCenter()
        {
            float totalX = 0;
            float totalY = 0;
            for (int i = 0; i < Shape.Length; i++)
            {
                totalX += Shape[i].X;
                totalY += Shape[i].Y;
            }

            return new Point(Convert.ToInt32(totalX / Shape.Length), Convert.ToInt32(totalY / Shape.Length));
        }

        public virtual double DistanceFromPoint(Point p)
        {
            Point closestPoint = GetClosestPoint(p);
            if(p != null)
            {
                return GetDistance(closestPoint, p);
            }
            return Double.MaxValue;
        }

        public virtual Point GetClosestPoint(Point p)
        {
            Point returner = new Point(Int32.MinValue, Int32.MinValue);
            double Distance = Double.MaxValue;
            double Holder;
            Point shapePoint;
            for(int i = 0; i < Shape.Length; i++) {
                shapePoint = Shape[i];
                Holder = GetDistance(p, shapePoint);
                if(Holder < Distance)
                {
                    Distance = Holder;
                    returner = shapePoint;
                    CornerIndex = i;
                }
            }

            return returner;
        }

        public static double GetDistance(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow(p2.X - p1.X, 2) + Math.Pow(p2.Y - p1.Y, 2));
        }

        public void CreateBaseShape(Point p)
        {
            List<Point> points = new List<Point>();

            points.Add(new Point(p.X - Convert.ToInt32(BaseWidth*.5), Convert.ToInt32(p.Y - BaseHeight*.5)));
            points.Add(new Point(p.X - Convert.ToInt32(BaseWidth * .5), Convert.ToInt32(p.Y + BaseHeight * .5)));
            points.Add(new Point(p.X + Convert.ToInt32(BaseWidth * .5), Convert.ToInt32(p.Y + BaseHeight * .5)));
            points.Add(new Point(p.X + Convert.ToInt32(BaseWidth * .5), Convert.ToInt32(p.Y - BaseHeight * .5)));

            Shape = points.ToArray();
        }

        public void Pan(Point change)
        {
            for(int i = 0; i< Shape.Length; i++) { 
                Shape[i].X += change.X;
                Shape[i].Y += change.Y;
            }
        }

        private void DrawVertex(Point p,Brush brush)
        {
            Point[] pointss = new Point[4];
            pointss[0] = new Point(p.X - DIST, p.Y - DIST);
            pointss[1] = new Point(p.X - DIST, p.Y + DIST);
            pointss[2] = new Point(p.X + DIST, p.Y + DIST);
            pointss[3] = new Point(p.X + DIST, p.Y - DIST);
            g.FillPolygon(brush, pointss);
        }

        public bool PointInside(Point point)
        {
            bool result = false;
            int j = Shape.Length - 1;

            for (int i = 0; i < Shape.Length; i++)
            {
                if (Shape[i].Y < point.Y && Shape[j].Y >= point.Y || Shape[j].Y < point.Y && Shape[i].Y >= point.Y)
                {
                    if (Shape[i].X + (point.Y - Shape[i].Y) / (Shape[j].Y - Shape[i].Y) * (Shape[j].X - Shape[i].X) < point.X)
                    {
                        result = !result;
                    }
                }
                j = i;
            }
            return result;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {

        }

        public void Dispose()
        {
            myBrush.Dispose();
            g.Dispose();
        }

        protected Point[] CopyShape()
        {
            List<Point> p = new List<Point>();

            for(int i = 0; i < Shape.Length; i++)
            {
                p.Add(new Point(Shape[i].X, Shape[i].Y));
            }

            return p.ToArray();
        }
        public abstract Drawable Copy();
    }
}
