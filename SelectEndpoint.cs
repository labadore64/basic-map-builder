﻿using MapBuilder.objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace MapBuilder
{
    public partial class SelectEndpoint : Form
    {

        public WarpDestination SelectedWarp
        {
            get;set;
        }

        public Map SelectedMap
        {
            get
            {
                if (Maps.Count > 0)
                {
                    return Maps[mapSelectedIndex];
                }
                return null;
            }
        }

        public BindingList<Map> Maps = new BindingList<Map>();
        public BindingList<WarpDestination> Warps = new BindingList<WarpDestination>();

        int mapSelectedIndex
        {
            get
            {
                if(MapListBox.SelectedIndex > 0)
                {
                    return MapListBox.SelectedIndex;
                }
                return 0;
            }
        }

        int warpSelectedIndex
        {
            get
            {
                if (DestinationListBox.SelectedIndex > 0)
                {
                    return DestinationListBox.SelectedIndex;
                }
                return 0;
            }
        }

        public DialogResult Result { get; set; }
        public SelectEndpoint()
        {
            InitializeComponent();
        }

        int MapSelectedIndex
        {
            get
            {
                if(MapListBox.SelectedIndex < 0)
                {
                    return 0;
                } else
                {
                    return MapListBox.SelectedIndex;
                }
            }
        }

        private void SelectEndpoint_Load(object sender, EventArgs e)
        {
            MapListBox.DataSource = Maps;
            SetDestList();
        }

        public void UpdateMapList(BindingList<Map> m)
        {
            Maps.Clear();
            foreach(Map mm in m)
            {
                Maps.Add(mm);
            }
            UpdateWarps();
            MapListBox.DataSource = Maps;
        }

        private void UpdateWarps()
        {
            Warps = new BindingList<WarpDestination>();
            foreach (WarpDestination dd in Maps[MapSelectedIndex].WarpDestinations)
            {
                Warps.Add(dd);
            }
            SetDestList();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Result = DialogResult.Cancel;
            Close();
        }

        private void MapListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateWarps();
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            SelectedWarp = (WarpDestination)DestinationListBox.SelectedItem;
            Result = DialogResult.OK;
            Close();
        }

        public void SetSelectedWarp()
        {
            //set the selected index to the proper warp destination.
            if (SelectedWarp != null)
            {
                MapListBox.SelectedIndex = Maps.IndexOf(SelectedWarp.ParentMap);
                SetDestList();


            }
        }

        private void DestinationListBox_Leave(object sender, EventArgs e)
        {
            SelectedWarp = (WarpDestination)DestinationListBox.SelectedItem;
        }

        private void SetDestList()
        {
            DestinationListBox.Items.Clear();
            List<WarpDestination> d = Maps[MapSelectedIndex].WarpDestinations;
            foreach(WarpDestination dd in d)
            {
                DestinationListBox.Items.Add(dd);
            }
            if (SelectedWarp != null)
            {
                DestinationListBox.SelectedIndex = DestinationListBox.Items.IndexOf(SelectedWarp);
            }
        }
    }
}
