﻿using System;
using System.Drawing;
using System.Runtime.Serialization;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace MapBuilder.objects
{
    public class AXLabelArea : ShapeObject
    {
        [XmlAttribute]
        public String Name { get; set; }
        [XmlAttribute]
        public String Description { get; set; }

        public AXLabelArea(Panel form)
        {
            ParentForm = form;
            Color = Color.Yellow;
            InitializeBrush();
        }

        public AXLabelArea()
        {
            Color = Color.Yellow;
        }

        public override Drawable Copy()
        {
            AXLabelArea a = new AXLabelArea(ParentForm);
            a.Name = Name;
            a.Description = Description;

            a.Shape = CopyShape();

            return a;
        }
    }
}
