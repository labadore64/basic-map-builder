﻿using MapBuilder.objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace MapBuilder
{
    public partial class Form1 : Form
    {
        static Origin origin;
        public static Point DrawRef {
            get
            {
                return Camera.Position;
            }

            set
            {
                Camera.Position = value;
            }
        }

        const double TEST_DISTANCE = 5;

        const int MOUSE_X_OFFSET = -8;
        const int MOUSE_Y_OFFSET = -30;

        const int COPY_OFFSET = 30;

        Point ClickPos;

        bool MousePressed = false;

        Map CurrentMap;
        BindingList<Map> MapList = new BindingList<Map>();

        Type SelectedType = null;

        //currently loaded map stuffs

        List<Collision> Collisions
        {
            get
            {
                return CurrentMap.Collisions;
            }
            set
            {
                CurrentMap.Collisions = value;
            }
        }
        List<AXLabel> Labels
        {
            get
            {
                return CurrentMap.Labels;
            }
            set
            {
                CurrentMap.Labels = value;
            }
        }
        List<AXLabelArea> AreaLabels
        {
            get
            {
                return CurrentMap.AreaLabels;
            }
            set
            {
                CurrentMap.AreaLabels = value;
            }
        }
        List<SoundEffect> Sounds
        {
            get
            {
                return CurrentMap.Sounds;
            }
            set
            {
                CurrentMap.Sounds = value;
            }
        }

        List<WarpEnter> WarpEnters
        {
            get
            {
                return CurrentMap.WarpEnters;
            }
            set
            {
                CurrentMap.WarpEnters = value;
            }
        }
        List<WarpDestination> WarpDestinations
        {
            get
            {
                return CurrentMap.WarpDestinations;
            }
            set
            {
                CurrentMap.WarpDestinations = value;
            }
        }

        Spawn Spawn
        {
            get
            {
                return CurrentMap.Spawn;
            }
            set
            {
                CurrentMap.Spawn = value;
            }
        }

        Drawable ActiveDrawable = null;
        Drawable SelectedDrawable = null;

        bool PanMode = false;

        Point LastMouse;

        public Form1()
        {
            InitializeComponent();
            CurrentMap = AddMap(new Map());
            listBox1.DataSource = MapList;
            origin = new Origin(panel1);
        }

        private Map AddMap(Map m)
        {
            MapList.Add(m);
            m.SetParentForm(panel1);
            return m;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void LeftClickDo(EventArgs e)
        {
            ClickPos = GetPosition();
            ClickPos.X -= DrawRef.X;
            ClickPos.Y -= DrawRef.Y;

            ActiveDrawable = GetClosestDrawableIfAble(ClickPos);

            if(ActiveDrawable != null)
            {
                ActiveDrawable.Selected = true;
            }

        }

        private void RightClickDo(EventArgs e)
        {
            ClickPos = GetPosition();
            ClickPos.X -= DrawRef.X;
            ClickPos.Y -= DrawRef.Y;

            Drawable test = GetClosestDrawableIfAble(ClickPos);
            ActiveDrawable = test;
            if (test == null)
            {
                bool cando = true;
                ShapeObject s;
                //tests if you're inside a collision
                for(int i = 0; i < Collisions.Count; i++)
                {
                    s = (ShapeObject)Collisions[i];
                    if (s.PointInside(ClickPos))
                    {
                        SelectedDrawable = s;
                        CollisionContextMenu.Show(this, GetPosition());
                        cando = false;
                        break;
                    }
                }

                //tests if you're inside an AX Label
                for (int i = 0; i < AreaLabels.Count; i++)
                {
                    s = (ShapeObject)AreaLabels[i];
                    if (s.PointInside(ClickPos))
                    {
                        SelectedDrawable = s;
                        AXLabelAreaContextMenu.Show(this, ClickPos);
                        cando = false;
                        break;
                    }
                }

                if (cando)
                {
                    //if there is no item nearby, open the menu.
                    MouseEventArgs mea = e as MouseEventArgs;
                    CreateItemMenu.Show(this, new Point(mea.X, mea.Y));
                }
            } else
            {
                //if item, open the context menu 
                SelectedDrawable = ActiveDrawable;
                MouseEventArgs mea = e as MouseEventArgs;
                Point p = new Point(mea.X, mea.Y);
                if (ActiveDrawable is AXLabel)
                {
                    AXLabelContextMenu.Show(this, p);
                }
                else if (ActiveDrawable is SoundEffect)
                {
                    SoundEffectContextMenu.Show(this, p);
                }
                else if (ActiveDrawable is Collision)
                {
                    CollisionContextMenu.Show(this, p);
                }
                else if (ActiveDrawable is AXLabelArea)
                {
                    AXLabelAreaContextMenu.Show(this, p);
                }
                else if (ActiveDrawable is WarpEnter)
                {
                    WarpAreaContextMenu.Show(this, p);
                }
                else if (ActiveDrawable is WarpDestination)
                {
                    WarpDestinationContextMenu.Show(this, p);
                }
            }
        }

        private void CreateCollisionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            StartCreateCollision(ClickPos);
            panel1.Refresh();
        }

        private void CreateAXLabelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StartCreateAXLabel(ClickPos);
            panel1.Refresh();
        }

        private void CreateAXLabelAreaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StartCreateAXLabelArea(ClickPos);
            panel1.Refresh();
        }

        private void CreateSoundEffectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StartCreateSoundEffect(ClickPos);
            panel1.Refresh();
        }

        private Point GetPosition()
        {
            
            return new Point(MousePosition.X-(Location.X + panel1.Location.X)+ MOUSE_X_OFFSET, MousePosition.Y-(Location.Y+panel1.Location.Y)+ MOUSE_Y_OFFSET);
        }

        private void StartCreateCollision(Point p)
        {
            Collision newLabel = new Collision(this.panel1);
            newLabel.CreateBaseShape(p);
            AddCollision(newLabel);
        }

        private void StartCreateAXLabel(Point p)
        {
            AXLabel newLabel = new AXLabel(this.panel1);
            newLabel.Position = p;
            AddLabel(newLabel);
        }

        private void StartCreateAXLabelArea(Point p)
        {
            AXLabelArea newLabel = new AXLabelArea(this.panel1);
            newLabel.CreateBaseShape(p);
            AddAreaLabel(newLabel);
        }

        private void StartCreateSoundEffect(Point p)
        {

            SoundEffect newLabel = new SoundEffect(this.panel1);
            newLabel.Position = p;
            AddSoundEffect(newLabel);
        }

        private void StartCreateWarpEnter(Point p)
        {

            WarpEnter newWarp = new WarpEnter(this.panel1);
            newWarp.CreateBaseShape(p);
            AddWarpEnter(newWarp);
        }
        private void StartCreateWarpDestination(Point p)
        {

            WarpDestination newWarp = new WarpDestination(this.panel1);
            newWarp.Position = p;
            AddWarpDestination(newWarp);
        }
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void Draw()
        {
            foreach (Drawable C in Collisions)
            {
                C.Draw();
            }
            foreach (Drawable A in AreaLabels)
            {
                A.Draw();
            }
            foreach(Drawable A in WarpEnters)
            {
                A.Draw();
            }
            foreach (Drawable L in Labels)
            {
                L.Draw();
            }
            foreach (Drawable S in Sounds)
            {
                S.Draw();
            }
            foreach(Drawable A in WarpDestinations)
            {
                A.Draw();
            }

            if(Spawn != null)
            {
                Spawn.Draw();
            }

            origin.Draw();
        }

        private Drawable GetClosestDrawableIfAble( Point p)
        {
            Drawable test;
            if (SelectedType == null)
            {
                foreach (Collision c in Collisions)
                {
                    test = DoGetClosest(c, p);
                    if (test != null)
                    {
                        return test;
                    }
                }
                foreach (AXLabelArea c in AreaLabels)
                {
                    test = DoGetClosest(c, p);
                    if (test != null)
                    {
                        return test;
                    }
                }
                foreach (WarpEnter c in WarpEnters)
                {
                    test = DoGetClosest(c, p);
                    if (test != null)
                    {
                        return test;
                    }
                }
                foreach (WarpDestination c in WarpDestinations)
                {
                    test = DoGetClosest(c, p);
                    if (test != null)
                    {
                        return test;
                    }
                }
                foreach (AXLabel c in Labels)
                {
                    test = DoGetClosest(c, p);
                    if (test != null)
                    {
                        return test;
                    }
                }
                foreach (SoundEffect c in Sounds)
                {
                    test = DoGetClosest(c, p);
                    if (test != null)
                    {
                        return test;
                    }
                }
                test = DoGetClosest(Spawn, p);
                if (test != null)
                {
                    return test;
                }
            } else
            {
                if(SelectedType == typeof(Collision))
                {
                    foreach (Collision c in Collisions)
                    {
                        test = DoGetClosest(c, p);
                        if (test != null)
                        {
                            return test;
                        }
                    }
                } else if (SelectedType == typeof(AXLabel))
                {
                    foreach (AXLabel c in Labels)
                    {
                        test = DoGetClosest(c, p);
                        if (test != null)
                        {
                            return test;
                        }
                    }
                }
                else if (SelectedType == typeof(AXLabelArea))
                {
                    foreach (AXLabelArea c in AreaLabels)
                    {
                        test = DoGetClosest(c, p);
                        if (test != null)
                        {
                            return test;
                        }
                    }
                }
                else if (SelectedType == typeof(SoundEffect))
                {
                    foreach (SoundEffect c in Sounds)
                    {
                        test = DoGetClosest(c, p);
                        if (test != null)
                        {
                            return test;
                        }
                    }
                }
                else if (SelectedType == typeof(WarpEnter))
                {
                    foreach (WarpEnter c in WarpEnters)
                    {
                        test = DoGetClosest(c, p);
                        if (test != null)
                        {
                            return test;
                        }
                    }
                }
                else if (SelectedType == typeof(WarpDestination))
                {
                    foreach (WarpDestination c in WarpDestinations)
                    {
                        test = DoGetClosest(c, p);
                        if (test != null)
                        {
                            return test;
                        }
                    }
                }
                else if (SelectedType == typeof(Spawn))
                {
                    test = DoGetClosest(Spawn, p);
                    if (test != null)
                    {
                        return test;
                    }
                }
            }

            return null;
        }

        private Drawable DoGetClosest(Drawable d, Point p)
        {
            PointObject test;
            if (d is PointObject)
            {
                test = (PointObject)d;
                if (test.DistanceFromPoint(p) < Math.Max(test.Radius, PointObject.BASE_RADIUS))
                {
                    return d;
                }
            }
            else
            {
                if (d is ShapeObject)
                {

                    ShapeObject o = (ShapeObject)d;

                    if (o.DistanceFromPoint(p) < TEST_DISTANCE)
                    {
                        o.CenterSelected = false;
                        return d;
                    }

                    if (ShapeObject.GetDistance(p, o.GetCenter()) < TEST_DISTANCE)
                    {
                        o.CenterSelected = true;
                        PanMode = true;
                        return d;
                    }

                    //test if click is anywhere in the shape.

                    if (o.PointInside(p))
                    {
                        o.CenterSelected = true;
                        PanMode = true;
                        return d;
                    }
                }

            }
            return null;
        }

        private void AddCollision(Collision c)
        {
            Collisions.Add(c);
        }
        
        private void AddSoundEffect(SoundEffect e)
        {
            Sounds.Add(e);
        }

        private void AddLabel(AXLabel a)
        {
            Labels.Add(a);
        }

        private void AddAreaLabel(AXLabelArea a)
        {
            AreaLabels.Add(a);
        }

        private void AddWarpEnter(WarpEnter a)
        {
            WarpEnters.Add(a);
        }

        private void AddWarpDestination(WarpDestination a)
        {
            WarpDestinations.Add(a);
            a.ParentMap = CurrentMap;
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            MouseEventArgs mea = e as MouseEventArgs;
            MousePressed = true;
            if (mea.Button == MouseButtons.Right)
            {
                RightClickDo(e);
            }
            else if (mea.Button == MouseButtons.Left)
            {
                LeftClickDo(e);
            } else if (mea.Button == MouseButtons.Middle)
            {
                Point CurrentPosition = GetPosition();
                CurrentPosition.X -= DrawRef.X;
                CurrentPosition.Y -= DrawRef.Y;
                ClickPos = CurrentPosition;
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            //put moving code here.
            MouseEventArgs mea = e as MouseEventArgs;
            Point CurrentPosition = GetPosition();
            if (mea.Button == MouseButtons.Left)
            {
                if (MousePressed)
                {
                    if(ActiveDrawable != null)
                    {
                        if(ActiveDrawable is PointObject)
                        {
                            CurrentPosition.X -= DrawRef.X;
                            CurrentPosition.Y -= DrawRef.Y;
                            PointObject p = (PointObject)ActiveDrawable;
                            p.Position = CurrentPosition;
                            panel1.Refresh();
                        } else if (ActiveDrawable is ShapeObject)
                        {
                            if (PanMode)
                            {
                                Point mousepos = CurrentPosition;
                                Point diff = new Point( mousepos.X - LastMouse.X, mousepos.Y - LastMouse.Y );

                                ShapeObject p = (ShapeObject)ActiveDrawable;
                                p.Pan(diff);
                                panel1.Refresh();
                            }
                            else
                            {
                                CurrentPosition.X -= DrawRef.X;
                                CurrentPosition.Y -= DrawRef.Y;
                                ShapeObject p = (ShapeObject)ActiveDrawable;
                                p.SelectedCorner = CurrentPosition;
                                panel1.Refresh();
                            }
                        }
                    }
                } 
            } else if(mea.Button == MouseButtons.Middle)
            {
                DrawRef = new Point(-ClickPos.X+ CurrentPosition.X, -ClickPos.Y + CurrentPosition.Y);
                panel1.Refresh();
            }
            LastMouse = CurrentPosition;
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            MousePressed = false;
            if(ActiveDrawable != null)
            {
                ActiveDrawable.Selected = false;
            }
            ActiveDrawable = null;
            PanMode = false;
            panel1.Refresh();
        }

        private void AXLabelContextMenu_Opening(object sender, CancelEventArgs e)
        {

        }

        private void RenameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AXLabel label = (AXLabel)SelectedDrawable;

            String result = DoTextBox(label.Name);
            if(result != null)
            {
                label.Name = result;
            }
        }

        private String DoTextBox(String originalText)
        {
            EnterText t = new EnterText();
            String TextResult = null;

            t.Text = originalText;
            t.ShowDialog();
            if (t.Result == DialogResult.OK)
            {
                TextResult = t.Text;
            }
            t.Dispose();
            return TextResult;
        }

        private void DeleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectedDrawable.Dispose();
            Labels.Remove((AXLabel)SelectedDrawable);

            SelectedDrawable = null;
            panel1.Refresh();
        }

        private void ToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            SelectedDrawable.Dispose();
            Sounds.Remove((SoundEffect)SelectedDrawable);

            SelectedDrawable = null;
            panel1.Refresh();
        }

        private void ToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            SelectedDrawable.Dispose();
            Collisions.Remove((Collision)SelectedDrawable);

            SelectedDrawable = null;
            panel1.Refresh();
        }

        private void UpdateDescriptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AXLabel label = (AXLabel)SelectedDrawable;

            String result = DoTextBox(label.Description);
            if (result != null)
            {
                label.Description = result;
            }
        }

        private void ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SoundEffect label = (SoundEffect)SelectedDrawable;

            String result = DoTextBox(label.Filename);
            if (result != null)
            {
                label.Filename = result;
            }
        }

        private void RenameToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AXLabelArea label = (AXLabelArea)SelectedDrawable;

            String result = DoTextBox(label.Name);
            if (result != null)
            {
                label.Name = result;
            }
        }

        private void ChangeDescriptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AXLabelArea label = (AXLabelArea)SelectedDrawable;

            String result = DoTextBox(label.Description);
            if (result != null)
            {
                label.Description = result;
            }
        }

        private void DeleteAXLabelAreaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectedDrawable.Dispose();
            AreaLabels.Remove((AXLabelArea)SelectedDrawable);

            SelectedDrawable = null;
            panel1.Refresh();
        }

        private void AddPoint(ShapeObject o, Point p)
        {
            o.AddPoint(p);
            panel1.Refresh();
        }

        private void RemovePoint(ShapeObject o)
        {
            o.RemovePoint(o.SelectedCorner);
            panel1.Refresh();
        }

        private void ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            AddPoint();
        }

        private void ToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            RemovePoint();
        }

        private void AddPointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddPoint();
        }

        private void RemovePointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemovePoint();
        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {
            Draw();
        }

        private void DrawDebug()
        {
            int Radius = 10;
            Point Position = GetPosition();
            Graphics g = panel1.CreateGraphics();

            g.DrawEllipse(Pens.White, Position.X - Radius, Position.Y - Radius, Radius * 2, Radius * 2);

            g.Dispose();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Map m = new Map();
            MapList.Add(m);
            CurrentMap = m;
            listBox1.SelectedIndex = MapList.IndexOf(CurrentMap);
            panel1.Refresh();
        }

        private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            CurrentMap = MapList[listBox1.SelectedIndex];
            CurrentMap.SetParentForm(panel1);
            panel1.Refresh();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if(MapList.Count > 1)
            {
                MapList.RemoveAt(listBox1.SelectedIndex);
                if(listBox1.SelectedIndex > 0)
                {
                    CurrentMap = MapList[listBox1.SelectedIndex - 1];
                } else
                {
                    CurrentMap = MapList[0];
                }
            }
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            String result = DoTextBox(CurrentMap.Name);
            if (result != null)
            {
                CurrentMap.Name = result;
            }
            Map m = new Map();
            MapList.Add(m);
            MapList.Remove(m);
            panel1.Refresh();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void SaveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            Serialize(saveFileDialog1.FileName);
        }

        private void Serialize(String filename)
        {
            TextWriter textWriter = new StreamWriter(@filename);
            try
            {
                XmlSerializer serial = new XmlSerializer(MapList.GetType());
                serial.Serialize(textWriter, MapList);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                textWriter.Close();
            }
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void Deserialize(String filename)
        {
            TextReader reader = new StreamReader(@filename);
            try
            {
                XmlSerializer serial = new XmlSerializer(MapList.GetType());
                MapList = (BindingList<Map>)serial.Deserialize(reader);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                reader.Close();
            }
            Map.SetIndex = MapList.Count;
            foreach (Map p in MapList)
            {
                p.SetParentForm(panel1);
                foreach(WarpEnter e in p.WarpEnters)
                {
                    e.GetWarpDestination(MapList);
                }
            }

            listBox1.DataSource = MapList;
            panel1.Refresh();
        }

        private void OpenFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            Deserialize(openFileDialog1.FileName);
        }

        private void CreateSelectWarpBox()
        {
            SelectEndpoint menu = new SelectEndpoint();
            WarpEnter e = (WarpEnter)SelectedDrawable;
            menu.SelectedWarp = e.WarpDestination;
            menu.UpdateMapList(MapList);
            menu.SetSelectedWarp();
            menu.ShowDialog();
            if (menu.Result == DialogResult.OK)
            {
                
                e.WarpDestination = menu.SelectedWarp;
                e.MapName = e.WarpDestination.ParentMap.Name;
                e.DestinationName = e.WarpDestination.ID;
            }
        }

        private void CreateWarpAreaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StartCreateWarpEnter(ClickPos);
            panel1.Refresh();
        }

        private void CreateWarpDestinationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StartCreateWarpDestination(ClickPos);
            panel1.Refresh();
        }

        private void DeleteDestinationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectedDrawable.Dispose();
            WarpDestinations.Remove((WarpDestination)SelectedDrawable);

            SelectedDrawable = null;
            panel1.Refresh();
        }

        private void DeleteWarpAreaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectedDrawable.Dispose();
            WarpEnters.Remove((WarpEnter)SelectedDrawable);

            SelectedDrawable = null;
            panel1.Refresh();
        }

        private void AddPointToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AddPoint();
        }

        private void AddPoint()
        {
            ShapeObject s = (ShapeObject)SelectedDrawable;

            AddPoint(s, GetPosition());
        }

        private void RemovePoint()
        {
            ShapeObject s = (ShapeObject)SelectedDrawable;

            if (s.Shape.Length > 3)
            {
                RemovePoint(s);
            }
        }

        private void DeletePointToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemovePoint();
        }

        private void ToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            WarpDestination label = (WarpDestination)SelectedDrawable;

            String result = DoTextBox(label.ID);
            if (result != null)
            {
                if (result.Length > 0)
                {
                    label.ID = result.Replace(" ", "_");
                }
            }
        }

        private void SetRadius(PointObject p)
        {
            String inputString = DoTextBox(p.Radius + "");

            if (float.TryParse(inputString, out float numValue))
            {
                p.Radius = numValue;
                panel1.Refresh();
            }
        }

        private void ToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            SetRadius((PointObject)SelectedDrawable);
        }

        private void ToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            SetRadius((PointObject)SelectedDrawable);
        }

        private void RadioAXLabel_CheckedChanged(object sender, EventArgs e)
        {
            if (radioAXLabel.Checked)
            {
                SelectedType = typeof(AXLabel);
            }
        }

        private void RadioAny_CheckedChanged(object sender, EventArgs e)
        {
            if (radioAny.Checked)
            {
                SelectedType = null;
            }
        }

        private void RadioAXLabelArea_CheckedChanged(object sender, EventArgs e)
        {
            if (radioAXLabelArea.Checked)
            {
                SelectedType = typeof(AXLabelArea);
            }
        }

        private void RadioCollision_CheckedChanged(object sender, EventArgs e)
        {
            if (radioCollision.Checked)
            {
                SelectedType = typeof(Collision);
            }
        }

        private void RadioSoundEffect_CheckedChanged(object sender, EventArgs e)
        {
            if (radioSoundEffect.Checked)
            {
                SelectedType = typeof(SoundEffect);
            }
        }

        private void RadioWarpEnter_CheckedChanged(object sender, EventArgs e)
        {
            if (radioWarpEnter.Checked)
            {
                SelectedType = typeof(WarpEnter);
            }
        }

        private void RadioWarpDestination_CheckedChanged(object sender, EventArgs e)
        {
            if (radioWarpDestination.Checked)
            {
                SelectedType = typeof(WarpDestination);
            }
        }

        private void RadioSpawn_CheckedChanged(object sender, EventArgs e)
        {
            if (radioSpawn.Checked)
            {
                SelectedType = typeof(Spawn);
            }
        }

        private void SetSpawnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(Spawn == null)
            {
                Spawn = new Spawn(panel1);
                Spawn.Position = ClickPos;
            } else
            {
                Spawn.Position = ClickPos;
            }
            panel1.Refresh();
        }

        private void ChangeDestinationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateSelectWarpBox();
        }

        private void ToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            AXLabel label = (AXLabel)SelectedDrawable;
            AXLabel newLabel = (AXLabel)label.Copy();
            newLabel.Position = new Point(newLabel.Position.X+ COPY_OFFSET, newLabel.Position.Y+ COPY_OFFSET);

            AddLabel(newLabel);
            SelectedDrawable = null;
            panel1.Refresh();
        }

        private void ToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            SoundEffect label = (SoundEffect)SelectedDrawable;
            SoundEffect newLabel = (SoundEffect)label.Copy();
            newLabel.Position = new Point(newLabel.Position.X + COPY_OFFSET, newLabel.Position.Y + COPY_OFFSET);

            AddSoundEffect(newLabel);
            SelectedDrawable = null;
            panel1.Refresh();
        }

        private void ToolStripMenuItem13_Click(object sender, EventArgs e)
        {
            WarpDestination label = (WarpDestination)SelectedDrawable;
            WarpDestination newLabel = (WarpDestination)label.Copy();
            newLabel.Position = new Point(newLabel.Position.X + COPY_OFFSET, newLabel.Position.Y + COPY_OFFSET);

            AddWarpDestination(newLabel);
            SelectedDrawable = null;
            panel1.Refresh();
        }

        private void ToolStripMenuItem11_Click(object sender, EventArgs e)
        {
            Collision label = (Collision)SelectedDrawable;
            Collision newLabel = (Collision)label.Copy();
            newLabel.Pan(new Point(COPY_OFFSET, COPY_OFFSET));

            AddCollision(newLabel);
            SelectedDrawable = null;
            panel1.Refresh();
        }

        private void ToolStripMenuItem14_Click(object sender, EventArgs e)
        {
            WarpEnter label = (WarpEnter)SelectedDrawable;
            WarpEnter newLabel = (WarpEnter)label.Copy();
            newLabel.Pan(new Point(COPY_OFFSET, COPY_OFFSET));

            AddWarpEnter(newLabel);
            SelectedDrawable = null;
            panel1.Refresh();
        }

        private void ToolStripMenuItem12_Click(object sender, EventArgs e)
        {
            AXLabelArea label = (AXLabelArea)SelectedDrawable;
            AXLabelArea newLabel = (AXLabelArea)label.Copy();
            newLabel.Pan(new Point(COPY_OFFSET, COPY_OFFSET));

            AddAreaLabel(newLabel);
            SelectedDrawable = null;
            panel1.Refresh();
        }
    }
}
