﻿using System;
using System.Drawing;
using System.Runtime.Serialization;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace MapBuilder.objects
{
    public class SoundEffect : PointObject
    {
        [XmlAttribute]
        public String Filename { get; set; }

        public SoundEffect(Panel form)
        {
            ParentForm = form;
            Color = Color.Magenta;
        }

        public SoundEffect()
        {
            Color = Color.Magenta;
        }

        public override Drawable Copy()
        {
            SoundEffect l = new SoundEffect();
            l.Filename = Filename;
            l.Radius = Radius;
            l.Position = CopyPoint();
            l.ParentForm = ParentForm;
            return l;
        }
    }
}
