﻿using System.Drawing;
using System.Windows.Forms;

namespace MapBuilder.objects
{
    public class Spawn : PointObject
    {
        public Spawn()
        {
            Color = Color.Red;
        }

        public Spawn(Panel p)
        {
            ParentForm = p;
            g = ParentForm.CreateGraphics();
        }

        public override Drawable Copy()
        {
            return null;
        }
    }
}
