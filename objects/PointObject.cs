﻿using System;
using System.Drawing;
using System.Runtime.Serialization;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace MapBuilder.objects
{
    public abstract class PointObject : Drawable
    {
        [XmlIgnore]
        public bool Selected { get; set; }
        [XmlIgnore]
        public Graphics g { get; protected set; }
        [XmlIgnore]
        public SolidBrush myBrush { get; protected set; }
        [XmlIgnore]
        public Color Color { get; protected set; } = Color.Red;
        [XmlIgnore]
        public Panel ParentForm { get; set; }

        public Point Position { get; set; }
        [XmlIgnore]
        public const int RECTANGLE_SIZE = 4;

        public const float BASE_RADIUS = 10;
        [XmlAttribute]
        public float Radius { get; set; } = BASE_RADIUS;

        public void Draw()
        {
            if (ParentForm != null)
            {
                Graphics g = ParentForm.CreateGraphics();
                if (!Selected)
                {
                    myBrush = new SolidBrush(Color);
                }
                else
                {
                    myBrush = new SolidBrush(Color.White);
                }
                g.DrawEllipse(Pens.Black, Position.X - Radius + Camera.Position.X, Position.Y - Radius + Camera.Position.Y, Radius * 2, Radius * 2);
                Rectangle r = new Rectangle(new Point(Convert.ToInt32(Position.X - RECTANGLE_SIZE * .5) + Camera.Position.X, Convert.ToInt32(Position.Y - RECTANGLE_SIZE * .5) + Camera.Position.Y), new Size(RECTANGLE_SIZE, RECTANGLE_SIZE));
                g.FillRectangle(myBrush, r);
            }

        }

		public virtual double DistanceFromPoint(Point p)
        {
            return GetDistance(Position, p);

        }
        private static double GetDistance(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow((p2.X - p1.X), 2) + Math.Pow((p2.Y - p1.Y), 2));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {

        }

        public void Dispose()
        {
            myBrush.Dispose();
            g.Dispose();
        }

        protected Point CopyPoint()
        {
            return new Point(Position.X, Position.Y);
        }

        public abstract Drawable Copy();
    }
}
