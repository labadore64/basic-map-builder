﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MapBuilder
{
    public partial class EnterText : Form
    {
        public String Text {
            get { return textBox1.Text; }
            set { textBox1.Text = value; }
        }
        public DialogResult Result { get; set; }

        public EnterText()
        {
            InitializeComponent();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Result = DialogResult.Cancel;
            Close();
        }

        private void EnterText_Load(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Result = DialogResult.OK;
            Close();
        }

    }
}
