﻿using System;
using System.Drawing;
using System.Runtime.Serialization;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace MapBuilder.objects
{
    public class AXLabel : PointObject
    {
        [XmlAttribute]
        public String Name { get; set; }
        [XmlAttribute]
        public String Description {get;set;}

        public AXLabel(Panel form)
        {
            ParentForm = form;
            Color = Color.Blue;
        }

        public AXLabel()
        {
            Color = Color.Blue;
        }

        public override Drawable Copy()
        {
            AXLabel l = new AXLabel();
            l.Name = Name;
            l.Description = Description;
            l.Radius = Radius;
            l.Position = CopyPoint();
            l.ParentForm = ParentForm;
            return l;
        }
    }
}
