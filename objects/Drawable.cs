﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MapBuilder.objects
{
    public interface Drawable : ISerializable
    {
        Graphics g { get; }
        SolidBrush myBrush { get; }

        bool Selected { get; set; }
        Color Color { get; }
        Panel ParentForm { get; }
        void Draw();
        void Dispose();

        Drawable Copy();
        double DistanceFromPoint(Point p);
    }
}
