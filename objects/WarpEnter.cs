﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace MapBuilder.objects
{
    public class WarpEnter : ShapeObject
    {
        [XmlIgnore]
        WarpDestination _dest;

        [XmlIgnore]
        public WarpDestination WarpDestination
        {
            get;set;
        }

        [XmlElement(ElementName = "Map")]
        public string MapName
        {
            get;set;
        }
        [XmlElement(ElementName="Destination")]
        public String DestinationName
        {
            get; set;
        }

        public void GetWarpDestination(BindingList<Map> maps)
        {
            foreach(Map m in maps)
            {
                if (m.Name.Equals(MapName))
                {
                    foreach(WarpDestination d in m.WarpDestinations)
                    {
                        if (d.ID.Equals(DestinationName))
                        {
                            WarpDestination = d;
                            WarpDestination.ParentMap = m;
                        }
                    }
                }
            }
        }

        public override Drawable Copy()
        {
            WarpEnter a = new WarpEnter(ParentForm);
            a.WarpDestination = WarpDestination;
            a.DestinationName = DestinationName;
            a.MapName = MapName;

            a.Shape = CopyShape();

            return a;
        }

        public WarpEnter()
        {
            Color = Color.LightPink;
        }

        public WarpEnter(Panel p)
        {
            Color = Color.LightPink;
            ParentForm = p;
            InitializeBrush();
        }
    }
}
