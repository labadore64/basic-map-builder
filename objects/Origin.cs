﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MapBuilder.objects
{
    public class Origin
    {
        SolidBrush brush = new SolidBrush(Color.White);
        Graphics g;
        Panel p;

        string drawString = "(0,0)";
        Font drawFont = new System.Drawing.Font("Arial", 12);
        StringFormat drawFormat = new System.Drawing.StringFormat();

        const float X_OFFSET = 15;
        const float Y_OFFSET = 25;
        const float COORD_SIZE = 30;
        const float COORD_WIDTH = 1;

        public Origin(Panel p)
        {
            this.p = p;
            g = p.CreateGraphics();
        }

        public void Draw()
        {
            g.DrawString(drawString, drawFont, brush, X_OFFSET+Camera.Position.X, Y_OFFSET + Camera.Position.Y, drawFormat);
            g.FillRectangle(brush,0-COORD_WIDTH + Camera.Position.X, 0- COORD_SIZE + Camera.Position.Y, COORD_WIDTH, COORD_SIZE*2);
            g.FillRectangle(brush, 0 - COORD_SIZE + Camera.Position.X, 0 - COORD_WIDTH + Camera.Position.Y, COORD_SIZE*2, COORD_WIDTH);
        }

        public void Dispose()
        {
            g.Dispose();
            brush.Dispose();
            drawFont.Dispose();
        }
    }
}
