﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace MapBuilder.objects
{
    public class WarpDestination : PointObject
    {
        [XmlAttribute]
        public String ID = "";
        public String MapName
        {
            get
            {
                if(ParentMap != null)
                {
                    return ParentMap.Name;
                }
                return "";
            }
        }

        static int counter;

        [XmlIgnore]
        public Map ParentMap { get; set; }
        public WarpDestination(Panel p)
        {
            Color = Color.Salmon;
            ParentForm = p;
            counter++;
            ID = "destination" + counter;
        }

        public WarpDestination()
        {
            Color = Color.Salmon;
        }

        public override string ToString()
        {
            return ID;
        }

        public override Drawable Copy()
        {
            WarpDestination l = new WarpDestination();
            l.ID = ID;
            l.ParentMap = ParentMap;
            l.Radius = Radius;
            l.Position = CopyPoint();
            l.ParentForm = ParentForm;
            return l;
        }
    }
}
